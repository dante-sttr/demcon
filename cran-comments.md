## R CMD check results

0 errors | 0 warnings | 0 note

* This package completed checks on the following systems:
    + Current R 4.2.2 on Windows 11
    + R-DEVEL on Windows 11
    + rocker/geospatial: latest on GitLab CI
    + rocker/geospateal: devel on GitLab CI

## General Note

This release was prompted by the release of the new version of V-Dem's dataset
and a note from CRAN maintainers to add informative error messages to functions
that access remote datasets. 

## Downstream dependencies

This package has no downstream dependencies.

## Functions

Functions with external download capabilities no longer download to the users
working or package directories.

  * Functions acquiring remote datasets (get_vdem, get_cce, get_polity5) now include 
  informative warning message when the HTTR request status code does not equal 200.

## Tests

Tests for V-Dem have been updated to reflect the version 13 structural changes.

Remaining warnings produced from tests are expected and designed to test downstream
effects after triggering a warning OR snapshot tests using vdiffr that are not expected
to work in those remote testing environments.

## Spelling

There are no spelling errors. The flagged NOTES are correct.

## Examples

All examples with less than a 5 second run time as indicated by devtools::check_rhub
had their \dontrun tags replaced with \donttest (8/31/22).

## URLs and Source Citations.

All current redirects have been updated using urlchecker. Remaining NOTES on url links are DOIs in
vignette reference sections. They are correct, just (naturally) include redirects.
