---
title: "Freedom House Freedom in the World"
output: github_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
check1 <- "states" %in% rownames(installed.packages())
check2 <- if (check1) packageVersion("states") > "0.2.2.9008" else FALSE
if (!all(check1, check2)) {
  stop("need dev version of states; please do\n   remotes::install_github(\"andybega/states\")")
}

suppressPackageStartupMessages({
  library(readxl)
  library(tidyverse)
  library(yaml)
  library(states)
})
devtools::load_all()

url <- "https://freedomhouse.org/sites/default/files/2020-02/2020_Country_and_Territory_Ratings_and_Statuses_FIW1973-2020.xlsx"

raw_path <- pitf.gravity::data_path("data-raw/data-cache", basename(url))
if (!file.exists(raw_path)) {
  download.file(url, destfile = raw_path)
}

# Read in raw data and fix column names
# Freedom House has a strange shift in survey periods under review during the
# 80s. For the 9 years after 1980 and before 1990, there are 8 surveys. The
# survey review periods shift from Jan. 1981 - Aug. 1982 to Aug. 1982 - Nov. 
# 1983 and then Nov. to Nov. after that until 1990. If you assign the split
# year surveys to whatever year they have more months in, then 1982 is left
# over without a survey. Fill that in with 1982 values, since the 1981-1982 
# survey covers 8 months of 1982. 
# Interesting, I've never used this data < 2003 (jb)
years <- rep(c(1972:1981, 1983:2019), each = 3)
raw_countries <- read_excel(raw_path, sheet = 2, skip = 2)
colstub <- stringr::str_extract(colnames(raw_countries), "^[A-Za-z]+")
colnames(raw_countries) <- c(
  "country_name",
  paste0(tail(colstub, -1), "-", years))

# Kosovo is already in country sheet; probably don't need these but if we do
# they introduce duplicate country-years!!!!
# this should be fixed by subsetting as needed. 
#
# raw_terr <- read_excel(raw_path, sheet = 3, skip = 2)
# colstub <- stringr::str_extract(colnames(raw_terr), "^[A-Za-z]+")
# colnames(raw_terr) <- c(
#   "country_name",
#   paste0(tail(colstub, -1), "-", years))
# 
# raw <- bind_rows(raw_countries, raw_terr)
raw <- raw_countries

# Start cleaning/formatting raw data
fh <- raw
# data.table::setDT(fh)
fh$cow <- countrycode::countrycode(fh$country_name, "country.name", "cown")
fh$cow[fh$country_name %in% c("Serbia", "Serbia and Montenegro")] <- 345L

# Fix some misclassifications in countrycode
fh$cow[fh$country_name=="Germany, W."] <- 260L
fh$cow[fh$country_name=="Germany, E."] <- 265L
fh$cow[fh$country_name=="Vietnam, S."] <- 817L
fh$cow[fh$country_name=="Yemen, S."] <- 680L
fh$cow[fh$country_name=="Yemen, N."] <- 678L


# What countries do not have a COW code?
fh %>% dplyr::filter(is.na(cow)) %>% dplyr::pull(country_name) %>% sort() %>%
  paste0(collapse = ", ") %>% cat()

# fh[is.na(cow), country_name]

# Good to drop
fh <- fh[!is.na(fh$cow), ]

fh <- fh %>%
  # collect everything together; one record per [cow, year, indicator] tuple
  tidyr::pivot_longer(-c(cow, country_name)) %>%
  tidyr::separate(name, into = c("indicator", "year")) 

# FH has missing (coded "-") values for country-years that are invalid/not indy countries
# need to remove all of those duplicates.
# E.g. FH has both Russia (365) and USSR (365), which COW assigns the same
# ID to. 

# Yugoslavia, S+M, Serbia
fh <- fh %>%
  dplyr::mutate(year = as.integer(year)) %>%
  dplyr::filter(!(country_name=="Yugoslavia" & year > 2001),
                !(country_name=="Serbia" & year < 2006),
                !(country_name=="Serbia and Montenegro" & (year < 2002 | year > 2005)))

# USSR/Russia
fh <- fh %>%
  dplyr::filter(!(country_name == "USSR" & year > 1989),
                !(country_name == "Russia" & year < 1990))

# N. Vietnam / Vietnam (post-unification)
fh <- fh %>%
  dplyr::filter(!(country_name == "Vietnam" & year < 1976),
                !(country_name == "Vietnam, N." & year > 1975))

# I moved this below the country subset so the tidyr::complete will create
# just one extra set of 1982s per cow making the replace command simpler (jb)

# 1982 is missing in the data because of the survey issue. Handle that problem
# now by adding 1982 year cases and filling them in with values from the 1981
# survey.
# (At this point the data is still a complete panel--all cases have years from
# 1972 to 2019: 
#   fh %>% group_by(cow) %>% summarize(min_year =min(year), max_year=max(year))
# )

# nifty...maybe the first time i've seen something without a direct data.table
# analog (jb) 

# Unless there was an unnoticed merge conflict/error, or I'm
# confused on the intent, this code was missing the indicator variable from the
# complete statement so only one observation per country was created instead of
# one for each level of indicator (jb - update)
fh <- fh %>%
  tidyr::complete(cow, year = min(year):max(year), indicator)

# Unless I deleted it, I do not see 1982 being filled in with 1981 
# I just read through the git diff and don't see the code. Doesn't matter
# for our temporal subset but for completionist sake I spent too long
# figuring out how to do this with tidyverse (jb)
fh <- fh %>%
  dplyr::group_by(cow, indicator) %>%
  dplyr::mutate(value = replace(value, year == 1982, value[year==1981]))

# I put this below my expansion and fill in to make sure I didn't mess it up
# check all are fixed (jb)
test <- nrow(fh %>% dplyr::group_by(cow, year) %>% dplyr::filter(dplyr::n() > 3))==0
stopifnot(
  "Duplicates still remaining" = test
)


# now get rid of country_name
fh$country_name <- NULL

# if this step gives a warning about non-unique rows, duplicates are 
# somehow still in the data
fh <- fh %>%
  # spread the indicators back to columns
  # one row per [cow, year] with columns for the three indicator
  tidyr::pivot_wider(names_from = "indicator", values_from = "value")

# The columns are right now still character, but before we convert:
# South Africa has weird values for 1972: 
# PR = 2(5); CL = 3(6); Status = F(NF)
# The years after that have 4, 5, PF; set the first year to that as well
# Otherwise these will become NA
# Nice catch, out of experience? (jb)
idx <- fh$cow==560 & fh$year==1972
# View(fh[idx,])
stopifnot("Index should only be TRUE for 1 record" = sum(idx)==1)
fh$PR[idx] <- "4"
fh$CL[idx] <- "5"
fh$Status[idx]  <- "PF"

# Fix column data types
# These are the nation-states prior to independence or other similar events
fh$PR[fh$PR=="-"] <- NA
fh$CL[fh$CL=="-"] <- NA
fh$Status[fh$Status=="-"] <- NA

# these should not give warnings about NA values
fh$PR <- as.integer(fh$PR)
fh$CL <- as.integer(fh$CL)

# Personally loathe factors until it's absolutely necessary to convert (jb)
fh$Status <- factor(fh$Status, levels = c("NF", "PF", "F"), ordered = TRUE)

# make column names distinct
fh <- fh %>%
  dplyr::rename(fh_pr = PR, fh_cl = CL, fh_status = Status)

# make it COW list compliant
master <- states::state_panel(1972L, max(fh$year), partial = "any", 
                             useGW = FALSE)
master <- dplyr::rename(master, cow = cowcode)

fh <- dplyr::left_join(master, fh, by = c("cow", "year"))

# Any countries missing all values?
fh %>%
  dplyr::group_by(cow) %>%
  dplyr::summarize(n = dplyr::n(), na = sum(is.na(fh_pr))) %>%
  dplyr::arrange(dplyr::desc(na/n)) %>%
  dplyr::filter(na > 0) 
# 987 is Micronesia
fh <- fh[!fh$cow==987L, ]
# the others look like just one or two gap years at beginning of indy
# carry-back/forward to fill those
fh <- fh %>%
  dplyr::group_by(cow) %>%
  dplyr::arrange(year) %>%
  tidyr::fill(c(fh_pr, fh_cl, fh_status), .direction = "downup") %>%
  dplyr::ungroup()

# we only need 2000 and on, but keep some extra years in case we lag
# Yes we lagged 1 year if I remember correct (jb)
fh <- fh[fh$year > 1998, ]

# Make sure cowcode and year are integer
fh$year <- as.integer(fh$year)
fh$cow  <- as.integer(fh$cow)

head(fh)
str(fh)

yaml::write_yaml(pitf.gravity::data_signature(fh), "signatures/fh.yml")
save(fh, file = pitf.gravity::data_path("data-clean/fh.rda"))
```

Data keys:

- PR is political rights
- CL is civil liberties
- Status has values:
    - F is free
    - PF is partially free
    - NF is not free
